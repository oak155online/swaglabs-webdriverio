/* global $ */

const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class InventoryPage extends Page {
  /**
     * define selectors using getter methods
     */
  get secondaryHeader () {
    return $('.header_secondary_container .title');
  }
}

module.exports = new InventoryPage();
