/* global browser */
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
  /**
     * define selectors using getter methods
     */
  get inputUsername () {
    return browser.getByPlaceholderText(/username/i);
  }

  get inputPassword () {
    return browser.getByPlaceholderText(/password/i);
  }

  get btnSubmit () {
    return browser.getByRole('button', {  name: /login/i})
  }

  /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
  async login (username, password) {
    await this.inputUsername.then((element) => {
      element.setValue(username);
    });
    await this.inputPassword.then((element) => {
      element.setValue(password);
    });
    await this.btnSubmit.then((element) => {
      element.click();
    });
  }

  /**
     * overwrite specific options to adapt it to page object
     */
  open () {
    return super.open('');
  }
}

module.exports = new LoginPage();
