/* global describe, expect, it */
const LoginPage = require('../pageobjects/login.page');
const InventoryPage = require('../pageobjects/inventory.page');

describe('My Login application', () => {
  it('should login with valid credentials', async () => {
    await LoginPage.open();

    await LoginPage.login('standard_user', 'secret_sauce');
    await expect(InventoryPage.secondaryHeader).toBeExisting();
    await expect(InventoryPage.secondaryHeader).toHaveTextContaining(
      'PRODUCTS');
  });
});


