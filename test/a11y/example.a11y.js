const AxeBuilder = require('@axe-core/webdriverio').default;
const { remote } = require('webdriverio');

(async () => {
  const client = await remote({
    logLevel: 'error',
    capabilities: {
      browserName: 'chrome'
    }
  });

  await client.url('https://www.saucedemo.com');

  const builder = new AxeBuilder({ client });
  try {
    const results = await builder.analyze();
    console.log(results);
  } catch (e) {
    console.error(e);
  } finally {
    client.closeWindow();
  }
})();
